## Requirements

For building and running the application you need:

- [JDK 11](https://openjdk.java.net/install/)
- [Maven 3](https://maven.apache.org)

---

## Running the application locally

```shell
mvn spring-boot:run
```

Go to http://localhost:8080

Role | User Name | Password | Allowed Operations |
--- | --- | --- | --- |
Guest | guest | pass0 | SEARCH_ROOMS, REQUEST_RESERVATION |
Rental Owner | owner | pass1 | MANAGE_ROOM_INVENTORY, APPROVE_CHECK_IN, APPROVE_CHECK_OUT |
Receptionist | receptionist | pass2 | APPROVE_CHECK_IN, APPROVE_CHECK_OUT |
Houser Keeper | keeper | pass3 | CLEANUP_ROOM |


---

## Project Structure

Project consists of: 

- Spring Boot
- [Typescript React App](https://create-react-app.dev/docs/adding-typescript/) located under src/main/ui

React App is integrated into Spring Boot via the [frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin). 

Spring boot serves React build as static content and provides rest endpoints protected with Spring Security using [json web token](https://jwt.io/).


Rest endpoints are documented in [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)