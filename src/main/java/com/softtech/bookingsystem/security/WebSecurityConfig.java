package com.softtech.bookingsystem.security;

import java.util.List;

import com.softtech.bookingsystem.models.EnumOperation;
import com.softtech.bookingsystem.security.jwt.AuthEntryPointJwt;
import com.softtech.bookingsystem.security.jwt.AuthTokenFilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		// securedEnabled = true,
		// jsr250Enabled = true,
		prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
	private AuthEntryPointJwt unauthorizedHandler;

  @Bean
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}
  
	private String[] toAuthorities(EnumOperation ...ops){
		return List.of(ops).stream().map(op -> op.name()).toArray(String[]::new);
	}
	@Bean
	@Override
	protected UserDetailsService userDetailsService() {

		UserDetails userGuest = User
		.withUsername("guest")
		.roles("GUEST")
		.authorities(this.toAuthorities(EnumOperation.LIST_ROOM, EnumOperation.REQUEST_RESERVATION))
		.passwordEncoder(passwordEncoder()::encode)
		.password("pass0")
		.build();

		UserDetails userReceptionist = User
		.withUsername("owner")
		.authorities(this.toAuthorities(EnumOperation.MANAGE_ROOM_INVENTORY, EnumOperation.APPROVE_CHECK_IN, EnumOperation.APPROVE_CHECK_OUT))
		.passwordEncoder(passwordEncoder()::encode)
		.password("pass1")
		.build();


		UserDetails userOwner = User
		.withUsername("receptionist")
		.authorities(this.toAuthorities(EnumOperation.APPROVE_CHECK_IN, EnumOperation.APPROVE_CHECK_OUT))
		.passwordEncoder(passwordEncoder()::encode)
		.password("pass2")
		.build();

		UserDetails userKeeper = User
		.withUsername("keeper")
		.authorities(this.toAuthorities(EnumOperation.CLEANUP_ROOM))
		.passwordEncoder(passwordEncoder()::encode)
		.password("pass3")
		.build();

		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(userGuest);
		manager.createUser(userReceptionist);
		manager.createUser(userOwner);
		manager.createUser(userKeeper);
		return manager;
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

  @Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.authorizeRequests()
			.antMatchers("/api/").authenticated()
			.anyRequest().permitAll();

		http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}
  
}
