package com.softtech.bookingsystem.payload.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SearchRoomRequest {
  private String[] cities;
  private String startDate;
  private String endDate;
  private RoomCapacity[] rooms;
}
