package com.softtech.bookingsystem.payload.request;

import java.time.LocalDate;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReservationRequest {
  private Set<Long> rooms;
  private String currency;
  private double price;
  private LocalDate inDate;
  private LocalDate outDate;
}
