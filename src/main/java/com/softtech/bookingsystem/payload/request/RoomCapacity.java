package com.softtech.bookingsystem.payload.request;

import lombok.Data;

@Data
public class RoomCapacity{
  private int adult;
}