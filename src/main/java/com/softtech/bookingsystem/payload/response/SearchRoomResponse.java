package com.softtech.bookingsystem.payload.response;

import com.softtech.bookingsystem.models.Hotel;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SearchRoomResponse {
  private Hotel hotel;
  private String startDate;
  private String endDate;
  private RoomResponse[] rooms;
}