package com.softtech.bookingsystem.payload.response;

import java.util.List;

import com.softtech.bookingsystem.models.EnumOperation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtResponse {
  private final String type = "Bearer";
  private String token;
  private String username;
  private List<EnumOperation> operations;
}
