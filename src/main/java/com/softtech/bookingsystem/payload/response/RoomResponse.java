package com.softtech.bookingsystem.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomResponse{
  private Long id;
  private String name;
  private int adultCapacity;
  private String currency;
  private double amount;
}

