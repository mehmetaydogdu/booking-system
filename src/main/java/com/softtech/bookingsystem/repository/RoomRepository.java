package com.softtech.bookingsystem.repository;

import java.util.List;

import com.softtech.bookingsystem.models.RoomType;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<RoomType, Long>, JpaSpecificationExecutor<RoomType> {
  @Query("select r from RoomType r, Hotel h where h=r.hotel and h.city in :cities and r.adultCapacity >= :adultCount")
  List<RoomType> findAllByCitiesAndAdultCount(@Param("adultCount") int adultCount, @Param("cities") String[] cities);

  @Query("select r from RoomType r, Hotel h where h=r.hotel and r.adultCapacity >= :adultCount")
  List<RoomType> findAllByAdultCount(@Param("adultCount") int adultCount);
}

class RoomSpecs {

  public static Specification<RoomType> adultCapacityGreaterThanOrEqual(int adultCount) {
    return (room, cq, cb) -> cb.greaterThanOrEqualTo(room.get("adultCapacity"), adultCount);
  }

}