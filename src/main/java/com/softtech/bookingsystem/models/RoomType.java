package com.softtech.bookingsystem.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;

@Data
@Entity(name = "RoomType")
@Table(name = "room_type")
@NoArgsConstructor
public class RoomType {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "room_type_generator")
  @SequenceGenerator(name = "room_type_generator", sequenceName = "room_type_seq", initialValue = 10, allocationSize = 1)
  private Long id;
  
  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private int totalCount;

  @Column(nullable = false)
  private int adultCapacity;

  @Column(nullable = false)
  private double price;

  @Column(nullable = false)
  private String currency;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "hotel_id")
  private Hotel hotel;
}
