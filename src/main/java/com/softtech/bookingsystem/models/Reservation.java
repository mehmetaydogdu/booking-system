package com.softtech.bookingsystem.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity(name = "reservation")
@Table(name = "reservation")
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_generator")
  @SequenceGenerator(name = "reservation_generator", sequenceName = "reservation_seq", initialValue = 10, allocationSize = 1)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User User;
  
  @Column(nullable = false)
  private double price;

  @Column(nullable = false)
  private String currency;

  @Enumerated(EnumType.ORDINAL)
  private ReservationStatus status;

}
