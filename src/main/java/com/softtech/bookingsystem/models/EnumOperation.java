package com.softtech.bookingsystem.models;

// Usage of Operations instead of Roles: https://ducmanhphan.github.io/2019-02-20-Problem-about-role-name-in-Spring-Security/ https://stackoverflow.com/questions/19525380/difference-between-role-and-grantedauthority-in-spring-security
// CAUTION: Enum values are used in React and also inserted into DB. In case you need to modify one, be sure that you run DB migration and update the React part.
public enum EnumOperation {
    LIST_ROOM,
    REQUEST_RESERVATION,
	MANAGE_ROOM_INVENTORY,
    APPROVE_CHECK_IN,
    APPROVE_CHECK_OUT,
    CLEANUP_ROOM
}
