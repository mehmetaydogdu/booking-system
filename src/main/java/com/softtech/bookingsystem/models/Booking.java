package com.softtech.bookingsystem.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity(name = "booking")
@Table(name = "booking")
@NoArgsConstructor
@AllArgsConstructor
public class Booking {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "booking_generator")
  @SequenceGenerator(name = "booking_generator", sequenceName = "booking_seq", initialValue = 10, allocationSize = 1)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "reservation_id")
  private Reservation reservation;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "room_id")
  private RoomType roomType;

  @Column(name = "in_date", columnDefinition = "DATE")
  private LocalDate inDate;

  @Column(name = "out_date", columnDefinition = "DATE")
  private LocalDate outDate;
  
}
