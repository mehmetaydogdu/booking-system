package com.softtech.bookingsystem.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@Data
@Entity(name = "Hotel")
@Table(name = "hotel")
@NoArgsConstructor
@AllArgsConstructor
public class Hotel {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hotel_generator")
  @SequenceGenerator(name = "hotel_generator", sequenceName = "hotel_seq", initialValue = 10, allocationSize = 1)
  private Long id;
  
  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String city;

  @Column(nullable = false)
  private String address;

  @Column()
  private int rating;

}
