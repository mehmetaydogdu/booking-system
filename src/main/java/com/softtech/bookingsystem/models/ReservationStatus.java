package com.softtech.bookingsystem.models;

public enum ReservationStatus {
  AWAITING_REVIEW, APPROVED, CANCELED;
}
