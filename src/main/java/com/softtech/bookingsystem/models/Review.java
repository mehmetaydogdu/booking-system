package com.softtech.bookingsystem.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;

@Data
@Entity(name = "Review")
@Table(name = "review")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Review {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "review_generator")
  @SequenceGenerator(name = "review_generator", sequenceName = "review_seq", initialValue = 10, allocationSize = 1)
  private Long id;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "hotel_id")
  private Hotel hotel;

  @Column(nullable = false)
  private String body;

  @Column(nullable = false)
  private int rating;

}
