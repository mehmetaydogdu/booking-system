package com.softtech.bookingsystem;

import com.softtech.bookingsystem.models.Hotel;
import com.softtech.bookingsystem.repository.HotelRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

// @Configuration
@Slf4j
class LoadDatabase {

  @Bean
  CommandLineRunner initDatabase(HotelRepository repository) {

    return args -> {
      log.info("Preloading " + repository.save(new Hotel(null, "Hilton", "İstanbul", "Ataşehir", 4)));
    };
  }
}
