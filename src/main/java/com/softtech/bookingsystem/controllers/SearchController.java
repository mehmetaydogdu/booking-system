package com.softtech.bookingsystem.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.http.HttpStatus;

import static java.util.stream.Collectors.groupingBy;

import java.time.LocalDate;

import com.softtech.bookingsystem.models.Booking;
import com.softtech.bookingsystem.models.Hotel;
import com.softtech.bookingsystem.models.Reservation;
import com.softtech.bookingsystem.models.ReservationStatus;
import com.softtech.bookingsystem.models.RoomType;
import com.softtech.bookingsystem.models.User;
import com.softtech.bookingsystem.payload.request.ReservationRequest;
import com.softtech.bookingsystem.payload.request.SearchRoomRequest;
import com.softtech.bookingsystem.payload.response.RoomResponse;
import com.softtech.bookingsystem.payload.response.SearchRoomResponse;
import com.softtech.bookingsystem.repository.BookingRepository;
import com.softtech.bookingsystem.repository.ReservationRepository;
import com.softtech.bookingsystem.repository.RoomRepository;
import com.softtech.bookingsystem.repository.UserRepository;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;


@RestController
@Slf4j
public class SearchController {
  
  private final RoomRepository roomRepository;
  private final BookingRepository bookingRepository;
  private final ReservationRepository reservationRepository;
  private final UserRepository userRepository;


  SearchController(RoomRepository roomRepository, BookingRepository bookingRepository, ReservationRepository reservationRepository, UserRepository userRepository) {
    this.roomRepository = roomRepository;
    this.bookingRepository = bookingRepository;
    this.reservationRepository = reservationRepository;
    this.userRepository = userRepository;
  }

  @PostMapping("/api/search")
  // ResponseEntity<CollectionModel<EntityModel<SearchRoomResponse>>> search(@RequestBody SearchRoomRequest searchRoom) {
  Stream<SearchRoomResponse> search(@RequestBody SearchRoomRequest searchRoom) {
    int maxAdultCount = Arrays.stream(searchRoom.getRooms()).mapToInt(r -> r.getAdult()).max().orElseGet(()-> 0);

    log.info("maxAdultCount: " +  maxAdultCount );

    List<RoomType> availableRooms = searchRoom.getCities().length==0 ? roomRepository.findAllByAdultCount(maxAdultCount) : roomRepository.findAllByCitiesAndAdultCount(maxAdultCount, searchRoom.getCities());

    // TO_DO filter out room_types which are all reserved during start and end dates of SearchRoomRequest

    Map<Hotel, List<RoomType>> roomsPerHotel = availableRooms.stream()
      .collect(groupingBy(RoomType::getHotel));
    
    Stream<SearchRoomResponse> rooms =  roomsPerHotel.entrySet().stream().map(entry -> {
      SearchRoomResponse resp =  new SearchRoomResponse();
      resp.setHotel(entry.getKey());
      resp.setRooms(entry.getValue().stream().map(r -> new RoomResponse(r.getId(), r.getName(), r.getAdultCapacity(), r.getCurrency(), r.getPrice())).toArray(RoomResponse[]::new));
      resp.setStartDate(searchRoom.getStartDate());
      resp.setEndDate(searchRoom.getEndDate());
      return resp;
    });
    return rooms;
  }

  @PostMapping("/api/book")
  @PreAuthorize("hasRole('GUEST')")
  @Transactional
  void reserve(@RequestBody ReservationRequest reservationRequest, Authentication authentication) {

    String username = authentication.getName();
    Optional<User> maybeUser = userRepository.findByUsername(username);
    if(maybeUser.isEmpty()) throw new InvalidReservationException();
    User user = maybeUser.get();

    // TO_DO check again if the rooms are still available and the price is still same if not throw exception with status code precondition failed 412
    double price =reservationRequest.getPrice();
    String currency = reservationRequest.getCurrency();
    LocalDate inDate = reservationRequest.getInDate();
    LocalDate outDate = reservationRequest.getOutDate();

    Set<Long> roomIds = reservationRequest.getRooms();
    List<RoomType> roomTypes = roomRepository.findAllById(roomIds);
    // TO_DO check roomIds exists in repository, if not throw InvalidReservationException

    Reservation reservation = new Reservation(null, user, price, currency, ReservationStatus.AWAITING_REVIEW);
    Reservation savedReservation = reservationRepository.save(reservation);

    Stream<Booking> bookingsToSave = roomTypes.stream().map(r -> new Booking(null, savedReservation, r, inDate, outDate));

    bookingRepository.saveAll(bookingsToSave.collect(Collectors.toList()));
    
  }
}

class InvalidReservationException extends RuntimeException {
  private static final long serialVersionUID = -6577588507560708023L;

  public InvalidReservationException() {
    super("Reservation request is not valid");
  }
}

@ControllerAdvice
class ExceptionHandlingAdvice {
  @ResponseBody
  @ExceptionHandler(InvalidReservationException.class)
  @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
  String invalidReservationHandler(final InvalidReservationException ex) {
    return ex.getMessage();
  }

}
