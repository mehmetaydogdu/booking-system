package com.softtech.bookingsystem.controllers;

import java.util.List;
import java.util.stream.Collectors;

import com.softtech.bookingsystem.models.EnumOperation;
import com.softtech.bookingsystem.payload.request.LoginRequest;
import com.softtech.bookingsystem.payload.response.JwtResponse;
import com.softtech.bookingsystem.security.jwt.JwtUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@RestController
@RequestMapping(value="/api/auth", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/signin")
  public ResponseEntity<JwtResponse> signIn(@Validated @RequestBody LoginRequest loginRequest) {
    Authentication authentication = authenticationManager
        .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    UserDetails userDetails = (UserDetails) authentication.getPrincipal();
    List<EnumOperation> operations = userDetails.getAuthorities().stream().map((item) -> {
      try {
        return EnumOperation.valueOf(item.getAuthority());
      } catch (Exception e) {
        log.error("authority:'{}' not recognized", item.getAuthority());
        return null;
      }
    }).filter(op -> op != null).collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), operations));

  }
}

@ControllerAdvice
class AuthenticationAdvice {
  
  @ResponseBody
  @ExceptionHandler(AuthenticationException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  String reportCountExceptionHandler(final AuthenticationException ex) {
    return ex.getMessage();
  }
}