import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { AppThunk } from ".";
import { SearchRoomResponse, User } from "../types";

const loggedOutUser:User|null  = null as User|null
const searchRoomResponse:Array<SearchRoomResponse> = []

const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: loggedOutUser,
    searchRoomResponse: searchRoomResponse,
    // result: blankResult,
    // isReportRunning: false,
    // isExportRunning: false,
    // tempReportTypePanelOptions: exractTempReportTypeOptions(initialReport),
    // tempColumnsPanelOptions: extractTempColumnsPanelOptions(initialReport),
    // tempHistoryOption: extractTempHistoryOption(initialReport)
  },
  reducers: {
    anonomizeUser(state) { state.user = loggedOutUser },
    setUser(state, action: PayloadAction<User>) { state.user = action.payload },
  }
})

export const {
  anonomizeUser, setUser
} = userSlice.actions


// export const runReport = (
//   page: number = -1,
// ): AppThunk => async (dispatch, getState) => {
//   dispatch(runReportStart())
//   const jiraService = JiraServiceFactory.get()
//   const pageNumber = page < 1 ? getState().report.result.pageNumber : page
//   const reportToExecute = _.cloneDeep(getState().report.active) as Report
//   const reportResult: ReportResult = await jiraService.search2(reportToExecute, false, pageNumber)
//   dispatch(setReportResult({ result: reportResult, executed: reportToExecute }))
//   dispatch(runReportEnd())
// }


export const reportReducer = userSlice.reducer