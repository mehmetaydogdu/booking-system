import { configureStore, combineReducers, Action } from '@reduxjs/toolkit'
import {reportReducer as userReducer} from './reducers'
import { ThunkAction } from 'redux-thunk'

const rootReducer = combineReducers({
  root:userReducer,
})

export type RootState = ReturnType<typeof rootReducer>

const store = configureStore({
  reducer: rootReducer,
});

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>
export type AppDispatch = typeof store.dispatch

export default store