export type User = {
  username: string,
  token:string,
  operations: Array<OPERATION>
}

export enum OPERATION {
  LIST_ROOM,
  REQUEST_RESERVATION,
  MANAGE_ROOM_INVENTORY,
  APPROVE_CHECK_IN,
  APPROVE_CHECK_OUT,
  CLEANUP_ROOM
}
export type Hotel = {
  id:number,
  name:string,
  city:string,
  address:string,
  rating:number,
}
export type Price = {
  amount:number,
  currency:string
}

export type Room = {
  id:number,
  name:string,
  adultCapacity:number,
} & Price

export type  SearchRoomResponse = {
  hotel: Hotel
  rooms:Array<Room>
}

export type SearchRoomRequest = {
  cities:Array<string>, inDate:string, outDate:string, rooms:Array<{adult:number}>
}

export class UnauthorizedError extends Error {
  constructor(message?: string) {
      super(message);
      // see: typescriptlang.org/docs/handbook/release-notes/typescript-2-2.html
      Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
      this.name = UnauthorizedError.name; // stack traces display correctly now 
  }
}