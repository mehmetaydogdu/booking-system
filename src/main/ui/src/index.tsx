import 'react-app-polyfill/stable';
import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import ReactLoading from "react-loading";
import '@atlaskit/css-reset';
import styled from 'styled-components';
import './index.css';
import App from './App';
import SearchPage from './SearchPage';
import { Provider } from 'react-redux';
import store from './store';


const Centered = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
  background-color: #282c34;
`;

const mainRouter =
  <Provider store={store}>
    <Suspense fallback={(<Centered><ReactLoading type="bubbles" color="#fff" /></Centered>)}>
      <BrowserRouter>
        <Route path="/" component={SearchPage} />
      </BrowserRouter>
    </Suspense>
  </Provider>

ReactDOM.render(mainRouter, document.getElementById('root'));
