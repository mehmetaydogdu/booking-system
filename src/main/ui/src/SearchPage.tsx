import moment from 'moment-timezone';
import React, { ReactElement, useEffect, useState } from 'react';
import { connect } from 'react-redux'
import { RootState } from './store';
import { gridSize as gridSizeFn } from '@atlaskit/theme'
import Page, { Grid, GridColumn } from '@atlaskit/page'
import PageHeader from '@atlaskit/page-header'
import Button, { ButtonGroup, LoadingButton } from '@atlaskit/button';
import { DatePicker } from '@atlaskit/datetime-picker';
import SelectPopupMulti from './components/SelectPopupMulti';
import SelectPopupMultiAsync from './components/SelectPopupMultiAsync';
import { WebService } from './WebService';
import { SearchRoomRequest, SearchRoomResponse } from './types';

import { ErrorMessage } from '@atlaskit/form';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import Form, { Field } from '@atlaskit/form';
import TextField from '@atlaskit/textfield';
import LoginForm from './LoginForm';
import DynamicTable from '@atlaskit/dynamic-table';

const gridSize = gridSizeFn();

const webService = new WebService()

type AppProps = {}

const timeZone = moment.tz.guess()

function SearchPage(props: AppProps) {
  const deafultInDate = moment().add(1, 'days').tz(timeZone).format("YYYY-MM-DD")
  const deafultOutDate = moment().add(3, 'days').tz(timeZone).format("YYYY-MM-DD")
  const [cities, setCities] = useState([{ label: 'İstanbul', value: 'İstanbul' }])
  const [isLoading, setIsLoading] = useState(false);
  const [inDate, setInDate] = useState(deafultInDate)
  const [outDate, setOutDate] = useState(deafultOutDate)
  const [user, setUser] = useState(webService.getStoredUser())
  const [loginError, setLoginError] = useState<null | string>(null)
  const [hotels, setHotels] = useState<Array<SearchRoomResponse>>([])

  const onSearchButtonClicked = () => {
    setIsLoading(true);
    const a: SearchRoomRequest = { cities: cities.map(c => c.value), inDate: inDate, outDate: outDate, rooms: [] }
    webService.searchRooms(a)
      .then(hotels => {
        setHotels(hotels)
        setIsLoading(false)
      })
      .catch(err => {
        setIsLoading(false)
      })
  }

  const onLogin = (username: string, password: string) => {
    webService.login(username, password).then(user => {
      setUser(user)
      setLoginError(null)
    })
      .catch(err => setLoginError('Bad credentials'))
  }

  const getBottomBar = () => {
    if (user === null) return <LoginForm onSubmit={onLogin} errorMessage={loginError} />

    const errorMessage = moment(inDate).isAfter(outDate) ? <div style={{ position: 'absolute' }}><ErrorMessage>Checkout date cannot be greater than latest checkin date.</ErrorMessage></div> : null;
    return (
      <Grid layout="fluid" spacing="comfortable">
        <GridColumn>
          <div style={{ padding: '20px 0px', margin: '-4px' }}>
            <SelectPopupMulti
              values={cities}
              options={['İstanbul', 'İzmir', 'Ankara'].map(c => ({ label: c, value: c }))}
              label='City'
              placeholder='Find Cities'
              onSelectedChange={(values) => setCities(values)}
            />
            <div style={{ display: 'inline-flex', margin: 4, verticalAlign: 'middle' }}>
              <div><div style={{ display: 'inline-flex', alignItems: 'center' }}><DatePicker
                spacing="compact"
                value={inDate}
                innerProps={{ style: { width: gridSize * 16 } }}
                locale={'tr_TR'}
                onChange={(v) => {
                  if (v) setInDate(v)
                  else setOutDate(deafultOutDate)
                }}
              /> -
            <DatePicker
                  spacing="compact"
                  value={outDate}
                  innerProps={{ style: { width: gridSize * 16 } }}
                  locale={'tr_TR'}
                  onChange={(v) => {
                    if (v) setOutDate(v)
                    else setOutDate(deafultOutDate)
                  }
                  }
                />
              </div>{errorMessage}</div>
            </div>
            {/* <SelectPopupMultiAsync
              values={users}
              options={[]}
              label='Deleted by'
              placeholder='Find Users'
              loadOptions={webService.getAssignees}
              onSelectedChange={(values) => setUsers(values)}
            />
            <div style={{ display: 'inline-flex', margin: 4, verticalAlign: 'middle' }}>
              <Select
                spacing="compact"
                styles={{
                  control: (base) => ({ ...base, width: gridSize * 12 }),
                  container: (base) => ({ ...base, marginRight: 4 }),
                }}
                components={{
                  DropdownIndicator: (props) =>
                    (<components.DropdownIndicator {...props}>
                      <DownIcon label="" size="small" />
                    </components.DropdownIndicator>)
                }}
                options={Object.keys(DateSelectTypes).map(k => DateSelectTypes[k]).map(v => ({ label: v, value: v }))}
                value={{ label: dateSelectType, value: dateSelectType }}
                onChange={(v) => setDateSelectType(DateSelectTypes[_.lowerCase(v?.['value'])])}
              />
              {datePickers()}
            </div> */}
            <div style={{ display: 'inline-flex', padding: '0px 8px', verticalAlign: 'middle' }}>
              <ButtonGroup>
                <LoadingButton appearance="primary" onClick={() => onSearchButtonClicked()} isLoading={isLoading}>Search</LoadingButton>
              </ButtonGroup>
            </div>
          </div>
        </GridColumn>
      </Grid>
    )
  }

  const rows = () => {
    return hotels.map(i => {
      const h = i.hotel
      const cells = [
        { key: h.name, content: h.name },
        { key: h.city, content: h.city },
        { key: h.id + '-' + i.rooms[0].name, content: <div>{i.rooms.map(r => <div>{r.name} - {r.adultCapacity} - {r.amount} {r.currency}</div>)}</div> },
        { key: 'book-action', content: <LoadingButton onClick={ () => {

        } }>Buy</LoadingButton> },
      ]
      return ({
        key: h.id,
        cells: cells,
        //onClick: (e: React.MouseEvent) => viewIssue(i.id)
      })
    })
  }
  return <div style={{ padding: `${gridSize * 5}px ${gridSize * 5}px` }}>
    <Page>
      {user !== null && <div>User:{user.username} Allowed operations: {user.operations.join(',')}</div>}
      <Grid layout="fluid">
        <GridColumn>
          <PageHeader
            truncateTitle={false}
            bottomBar={getBottomBar()}
            actions={<ButtonGroup>
              {user !== null &&
                <Button appearance='primary' onClick={() => {
                  localStorage.removeItem('user')
                  setUser(null)
                }}>Logout</Button>}
            </ButtonGroup>}
          >
            Hotel boooking system
        </PageHeader>
        </GridColumn>
      </Grid>
      {user !== null && <Grid layout="fluid">
        <GridColumn>
          <DynamicTable
            caption=''
            head={{
              cells: [
                { key: 'Hotel', content: 'Hotel', isSortable: true },
                { key: 'City', content: 'City', isSortable: true },
                { key: 'Rooms', content: 'Rooms - Capacity - Price ', isSortable: false },
                { key: 'Action', content: 'Action', isSortable: false },
              ]
            }}
            // @ts-ignore
            rows={rows()}
            loadingSpinnerSize="large"
            isLoading={isLoading}
            rowsPerPage={50}
            defaultPage={1}
            emptyView={<div>No room found</div>}
          />
        </GridColumn>
      </Grid>
      }
    </Page>
  </div>
}

const selectUser = (state: RootState) => state.root.user
const mapStateToProps = (state: RootState) => ({
  user: selectUser(state),
  searchRoomResponse: state.root.searchRoomResponse
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps, mapDispatchToProps
)(SearchPage);