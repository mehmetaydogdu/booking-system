import { SearchRoomRequest, SearchRoomResponse, UnauthorizedError, User } from "./types";

export class WebService {
  getStoredUser = (): User | null => {
    const userStr = localStorage.getItem('user');

    if (userStr) {
      const user = JSON.parse(userStr)
      return user
    } else {
      return null
    }
  }

  searchRooms = (searchRoomRequest: SearchRoomRequest): Promise<Array<SearchRoomResponse>> => {
    const user = this.getStoredUser()
    if (user === null) throw new UnauthorizedError()

    return fetch(`/api/search`, {
      headers: { Authorization: 'Bearer ' + user.token, 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(searchRoomRequest)
    }).then(res => {
      if (res.status === 401) {
        throw new UnauthorizedError()
      }
      if (res.status === 200) {
        return res.json()
      }
      throw new UnauthorizedError()
    })
  }
  login = (username: string, password: string): Promise<User> => {
    const data = { username: username, password: password }
    return fetch(`/api/auth/signin`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(data)
    }).then(res => {
      if (res.status === 401) {
        throw new UnauthorizedError()
      }
      if (res.status === 200) {
        return res.json().then(user => {
          localStorage.setItem('user', JSON.stringify(user));
          return user;
        })
      } else {
        throw new UnauthorizedError()
      }
    })
  }
}