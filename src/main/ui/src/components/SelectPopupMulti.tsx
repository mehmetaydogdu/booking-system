import _ from 'lodash';
import React, { ReactNode } from 'react';
import { PopupSelect, OptionsType, OptionType, CheckboxOption, createFilter } from '@atlaskit/select';
import DownIcon from '@atlaskit/icon/glyph/hipchat/chevron-down';
import Button from '@atlaskit/button';
import { colors, gridSize } from '@atlaskit/theme';
import QuestionCircleIcon from '@atlaskit/icon/glyph/question-circle';
import { withTranslation, WithTranslation } from 'react-i18next';
import JiraFailedBuildStatusIcon from '@atlaskit/icon/glyph/jira/failed-build-status';
import Tooltip from '@atlaskit/tooltip';

const CLEAR_VALUE="__clear-selected__";

const ClearOption = ({ children, innerProps, isFocused }: any) => (
  <div
    style={{
      boxSizing: 'border-box',
      color: colors.primary(),
      cursor: 'pointer',
      fontSize: 'inherit',
      padding: '8px 12px',
      userSelect: 'none',
      textDecoration: isFocused ? 'underline' : null,
      WebkitTapHighlightColor: 'rgba(0,0,0,0)',
      width: '100%',

      '&:hover': {
        textDecoration: 'underline',
      },
    }}
    {...innerProps}
  >
    {children}
  </div>
);

const CheckOption = (props: any) => (
  <CheckboxOption style={{ paddingLeft: `8px !important` }} {...props} />
);

const Option = (props: any) => props.data.value === CLEAR_VALUE ? ( <ClearOption {...props} />) : ( <CheckOption {...props} /> );

const Control = ({ children, innerProps, innerRef }: any) => (
  <div
    ref={innerRef}
    style={{
      boxShadow: `0 2px 0 ${colors.N30A}`,
      display: 'flex',
      padding: 4,
    }}
    {...innerProps}
  >
    {children}
  </div>
);

type BoxProps = { children: ReactNode, height: number };
const Box = ({ height, ...props }: BoxProps) => (
  <div
    style={{
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
      height,
      justifyContent: 'center',
    }}
    {...props}
  />
);

Box.defaultProps = { height: 140 };
const Text = (props: any) => (
  <div
    style={{
      fontWeight: 500,
      fontSize: '0.85rem',
      color: colors.N100,
      marginTop: gridSize() * 2,
    }}
    {...props}
  />
);
const NoOptionsMessage = () => (
  <Box>
    <QuestionCircleIcon label="" primaryColor={colors.N100} size="xlarge" />
    <Text>No matches found</Text>
  </Box>
);

const selectComponents = {
  Control,
  IndicatorSeparator: null,
  //LoadingIndicator: null,
  NoOptionsMessage,
  Option,
};

const defaultFilterOptions = createFilter({
  ignoreCase: true,
  ignoreAccents: true,
  stringify: option => option.label + " " + option.value,
  trim: true,
  matchFrom: 'any'
})

const filterOptions = (option, rawInput) => {
  if (option.value === CLEAR_VALUE && rawInput) {
    return false;
  }
  return defaultFilterOptions(option, rawInput);
};

type SelectStates = { options:OptionsType };
type SelectProps = { options: OptionsType, label:string, placeholder: string, values:OptionsType, onSelectedChange:(values:any)=>void, formatOptionLabel?: (option: OptionType) => React.ReactNode } & WithTranslation;
class SelectPopupMulti extends React.Component<SelectProps, SelectStates>{
  state = {
    options: this.props.options,
  };

  onMenuOpen = () => {
    // set options here ONCE when the dialog opens, so they don't jostle about
    // as users select/deselect values
    this.setState({
      options:this.getOptions(this.props.values, this.props.options)
    });
  }
  onChange = (values: any) => {
    const  newValues = values && Array.isArray(values) && values.map(x => x.value).includes(CLEAR_VALUE) ? [] : values;
    // if (values && Array.isArray(values) && values.map(x => x.value).includes(CLEAR_VALUE)) {
    //   this.setState({
    //     values:[],
    //   });
    // } else {
    //   this.setState({
    //     values:values,
    //   });
    // }
    this.props.onSelectedChange(newValues)
  };
  getOptions = (top:OptionsType, all:OptionsType) => {
    const { t } = this.props;
    if (!top || !top.length) return all;

    const notSelected = all.map(x => {
      if(x.options){//in case of group of options as in issue types
        return ({...x, options:x.options.filter(v=> !top.some(o=> o.value===v.value))})
      }else{
        return top.some(o => o.value===x.value) ? [] : [x]
      }
    }).flat()

    const clearOption = [{ value: CLEAR_VALUE, label: t('Clear selected items') }];

    return top
      .concat(clearOption)
      .concat(notSelected);
  };

  formatOptionLabel = (data: any) => {
    const { t } = this.props;
    const isValidOption = () => {
      if(data.value === CLEAR_VALUE) return true;
      const leafOptions = this.props.options.flatMap(x =>{
        if(x.options) return x.options //in case of group of options as in issue types
        else return x
      })
      return leafOptions.some(o => o.value===data.value)
    } 
    const tooltip = isValidOption() ? null : (
    <Tooltip content={`${this.props.label} '${data.value}' ${t("is not valid for your current Project(s) selection")}`}>
      <JiraFailedBuildStatusIcon label='' size='medium' primaryColor={colors.N0} secondaryColor={colors.R400}></JiraFailedBuildStatusIcon>
    </Tooltip>)
    return (<div style={{ alignItems: 'center', display: 'flex'}}>
      <div style={{marginRight: 'auto'}}>
        {this.props.formatOptionLabel && data.value !== CLEAR_VALUE? this.props.formatOptionLabel(data) : data.label}
      </div>
      {tooltip}
    </div>
    )
  };
  formatLabel = (values:OptionsType) => {
    const { t } = this.props;
    const separator = ', ';
    const max = 3;
    const makeLabel = suffix => (
      <span>
        <strong>{this.props.label}:</strong> {suffix}
      </span>
    );
    // no value
    if (values.length === 0) return this.props.label + ': '+ t('All');

    // create comma separated list of values
    // maximum 3 visible
    const valueMap = values.map(v => _.isString(v.label) ? v.label : v.value);
    const valueLength = valueMap.length;
    return makeLabel(
      valueLength > max
        ? `${valueMap.slice(0, max).join(separator)} +${valueLength - max} ${t('more')}`
        : valueMap.join(separator),
    );
  }
  render = () => {
    const label = this.formatLabel(this.props.values)
    return (
      <PopupSelect
        options={this.state.options}
        placeholder={this.props.placeholder}
        backspaceRemovesValue={false}
        closeMenuOnSelect={false}
        controlShouldRenderValue={false}
        hideSelectedOptions={false}
        isClearable={false}
        isMulti
        menuIsOpen
        menuShouldScrollIntoView={false}
        tabSelectsValue={false}
        filterOption={filterOptions}
        components={selectComponents}
        
        formatOptionLabel={this.formatOptionLabel}
        onChange={this.onChange}
        onMenuOpen={this.onMenuOpen}
        value={this.props.values}
        target={({ ref, isOpen }) => <Button ref={ref} style={{margin:"4px"}} isSelected={isOpen} iconAfter={<DownIcon label="" size="small"/>}>{label}</Button>}
      />
    );
  }
}
export default withTranslation()(SelectPopupMulti)