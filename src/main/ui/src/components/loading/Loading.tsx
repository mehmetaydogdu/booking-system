import React from 'react';
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import * as legoData from "./legoloading.json";
import * as doneData from "./doneloading.json";
import "./Loading.css";

//https://github.com/deeayeen/reactuxmediumtest

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: legoData['default'],
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice"
  }
};
const defaultOptions2 = {
  loop: false,
  autoplay: true,
  animationData: doneData['default'],
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice"
  }
};

export default class Loading extends React.Component<{stopLoading:boolean, message:string}, {}>{
  static defaultProps = {
    stopLoading: false,
    message:'Loading'
  }
  render() {
    return <div className='Loading'>
                <FadeIn>
                  <div className="d-flex justify-content-center align-items-center">
                    <h1 className='Loading-header'>{this.props.message}</h1>
                    {this.props.stopLoading? (<Lottie options={defaultOptions2} height={120} width={120} />) :(<Lottie options={defaultOptions} height={120} width={120} />)}
                  </div>
                </FadeIn>
           </div>
  }
}