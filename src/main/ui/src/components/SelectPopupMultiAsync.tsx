import _ from 'lodash';
import React, { ReactNode } from 'react';
import { OptionsType, CheckboxOption, createFilter } from '@atlaskit/select';
import DownIcon from '@atlaskit/icon/glyph/hipchat/chevron-down';
import Button from '@atlaskit/button';
import { colors, gridSize } from '@atlaskit/theme';
import PopupAsyncSelect from './from-atlaskit/PopupSelect/PopupSelect'; 
import { ActionMeta } from './from-atlaskit/types';
import Avatar from '@atlaskit/avatar';
import { withTranslation, WithTranslation } from 'react-i18next';

const CLEAR_VALUE="__clear-selected__";

const ClearOption = ({ children, innerProps, isFocused }: any) => (
  <div
    style={{
      boxSizing: 'border-box',
      color: colors.primary(),
      cursor: 'pointer',
      fontSize: 'inherit',
      padding: '8px 12px',
      userSelect: 'none',
      textDecoration: isFocused ? 'underline' : null,
      WebkitTapHighlightColor: 'rgba(0,0,0,0)',
      width: '100%',

      '&:hover': {
        textDecoration: 'underline',
      },
    }}
    {...innerProps}
  >
    {children}
  </div>
);

const CheckOption = (props: any) => (
  <CheckboxOption style={{ paddingLeft: `8px !important` }} {...props} />
);

const Option = (props: any) => props.data.value === CLEAR_VALUE ? ( <ClearOption {...props} />) : ( <CheckOption {...props} /> );

const Control = ({ children, innerProps, innerRef }: any) => (
  <div
    ref={innerRef}
    style={{
      boxShadow: `0 2px 0 ${colors.N30A}`,
      display: 'flex',
      padding: 4,
    }}
    {...innerProps}
  >
    {children}
  </div>
);

type BoxProps = { children: ReactNode, height: number };
const Box = ({ height, ...props }: BoxProps) => (
  <div
    style={{
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
      height,
      justifyContent: 'center',
    }}
    {...props}
  />
);

Box.defaultProps = { height: 140 };

const selectComponents = {
  Control,
  IndicatorSeparator: null,
  //LoadingIndicator: null,
  Option,
};

const defaultFilterOptions = createFilter({
  ignoreCase: true,
  ignoreAccents: true,
  stringify: option => option.label + " " + option.value,
  trim: true,
  matchFrom: 'any'
})

const filterOptions = (option, rawInput) => {
  if (option.value === CLEAR_VALUE && rawInput) {
    return false;
  }
  return defaultFilterOptions(option, rawInput);
};

const formatOptionLabel = (data: any) => (
  <div style={{ alignItems: 'center', display: 'flex' }}>
    {data.value === CLEAR_VALUE ? null : <Avatar src={data.avatar} size="xsmall" />}
    <div style={{ marginLeft: gridSize() }}>{data.label}</div>
  </div>
);

type SelectStates = {values:OptionsType, options:OptionsType};
type SelectProps = { options: OptionsType, label:string, placeholder: string, loadOptions:((string)=>Promise<OptionsType>), values:OptionsType, onSelectedChange:(values:any)=>void } & WithTranslation;
class SelectPopupMultiAsync extends React.Component<SelectProps, SelectStates>{
  state = {
    menuIsOpen:true,
    values:  this.props.values,
    options: this.props.options,
  };

  selectRef = React.createRef<PopupAsyncSelect>();
  onMenuOpen = () => {
    // set options here ONCE when the dialog opens, so they don't jostle about
    // as users select/deselect values
    this.setState({
      options:this.getOptions(this.state.values, this.props.options)
    });
  }

  onChange = (values: any, actionMeta: ActionMeta<any>) => {
    const updateOptions=(xs:any)=>{
      // Do not update options on deselect
      if(actionMeta.action === 'deselect-option') return this.state.options;
      else if(actionMeta.action === 'select-option' && values.every(v => this.state.options.includes(v))){ // Do not update if selected value is already in options
        return this.state.options;
      }else return this.getOptions(xs, this.props.options)
    }
    if (values && Array.isArray(values) && values.map(x => x.value).includes(CLEAR_VALUE)) {
      this.setState({
        values:[],
        options:updateOptions([])
      });
      this.props.onSelectedChange([]);
    } else {
      this.setState({
        values:values,
        options:updateOptions(values)
      });
      this.props.onSelectedChange(values)
    }
  };
  getOptions = (top:OptionsType, all:OptionsType) => {
    const { t } = this.props;
    if (!top || !top.length) return all;

    const notSelected = all.map(x => {
      if(x.options){//in case of group of options as in issue types
        return ({...x, options:x.options.filter(v=> !top.some(o=> o.value===v.value))})
      }else{
        return top.some(o => o.value===x.value) ? [] : [x]
      }
    }).flat();

    const clearOption = [{ value: CLEAR_VALUE, label: t('Clear selected items') }];

    return top
      .concat(clearOption)
      .concat(notSelected);
  };

  formatLabel = (values:OptionsType) => {
    const { t } = this.props;
    const separator = ', ';
    const max = 3;
    const makeLabel = suffix => (
      <span>
        <strong>{this.props.label}:</strong> {suffix}
      </span>
    );
    // no value
    if (values.length === 0) return this.props.label + ': '+ t('All');;

    // create comma separated list of values
    // maximum 3 visible
    const valueMap = values.map(v => _.isString(v.label) ? v.label : v.value);
    const valueLength = valueMap.length;
    return makeLabel(
      valueLength > max
        ? `${valueMap.slice(0, max).join(separator)} +${valueLength - max} ${t('more')}`
        : valueMap.join(separator),
    );
  }

  render = () => {
    const { t } = this.props;
    const label = this.formatLabel(this.state.values)
    return (
      <PopupAsyncSelect
        ref={this.selectRef} 
        searchThreshold={-1}
        loadOptions={this.props.loadOptions}
        defaultOptions={this.state.options}

        placeholder={this.props.placeholder}
        backspaceRemovesValue={false}
        closeMenuOnSelect={false}
        controlShouldRenderValue={false}
        hideSelectedOptions={false}
        isClearable={false}
        isMulti
        menuIsOpen = {this.state.menuIsOpen}
        menuShouldScrollIntoView={false}
        tabSelectsValue={false}
        filterOption={filterOptions}
        components={selectComponents}
        loadingMessage={({ inputValue: string }) => t('Loading...')}
        formatOptionLabel={formatOptionLabel}

        onChange={this.onChange}
        onOpen={this.onMenuOpen}
        value={this.state.values}
        target={({ ref, isOpen }) => <Button ref={ref} style={{margin:"4px"}} isSelected={isOpen} iconAfter={<DownIcon label="" size="small"/>}>{label}</Button>}
      />
    );
  }
}
export default withTranslation()(SelectPopupMultiAsync)